﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class ProductsController : ApiController
    {
        Product[] products = new Product[]
        {
            new Product { Id = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1 },
            new Product { Id = 2, Name = "Yo-yo", Category = "Toys", Price = 3.75M },
            new Product { Id = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M }
        };

        public IEnumerable<Product> GetAllProducts()
        {
            return products;
        }

        public IHttpActionResult GetProduct(int id)
        {
            var product = products.FirstOrDefault((p) => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }

        [HttpPost]
        public void Post([FromBody] person p)
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "172.16.189.102";
                builder.UserID = "sa";
                builder.Password = "pass@123";
                builder.InitialCatalog = "master";

                Console.Write("Connecting to SQL Server ... ");
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    String sql = "";
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Use XamarinDB; ");
                    sb.Append("INSERT into Student (Name, CollegeName, Stream, PhoneNumber, PassOutYear, Sem1Percentage, Sem2Percentage, Sem3Percentage) ");
                    sb.Append("VALUES (@name, @collegename, @stream, @phonenumber, @passoutyear, @sem1percentage, @sem2percentage, @sem3percentage);");
                    sql = sb.ToString();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@name", p.name);
                        command.Parameters.AddWithValue("@collegename", p.collegename);
                        command.Parameters.AddWithValue("@stream", p.stream);
                        command.Parameters.AddWithValue("@phonenumber", p.phonenumber);
                        command.Parameters.AddWithValue("@passoutyear", p.passoutyear);
                        command.Parameters.AddWithValue("@sem1percentage", p.sem1percentage);
                        command.Parameters.AddWithValue("@sem2percentage", p.sem2percentage);
                        command.Parameters.AddWithValue("@sem3percentage", p.sem3percentage);
                        command.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }

    public class person
    {
        public string name { get; set; }
        public string stream { get; set; }
        public Int64 phonenumber { get; set; }
        public string collegename { get; set; }
        public int passoutyear { get; set; }
        public float sem1percentage { get; set; }
        public float sem2percentage { get; set; }
        public float sem3percentage { get; set; }
    }
}
