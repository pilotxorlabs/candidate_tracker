﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SaveDataClient
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
        }

        public void SaveData(object sender, EventArgs e)
        {
            if(!ValidateInputs())
            {
                return;
            }
            person p = new person();
            p.name = name.Text;
            p.stream = stream.Text;
            p.collegename = college.Text;
            p.passoutyear = Int32.Parse(passoutyear.Text);
            p.phonenumber = Int64.Parse(phonenumber.Text);
            p.sem1percentage = (float)(Math.Round(float.Parse(sem1marks.Text), 2));
            p.sem2percentage = (float)(Math.Round(float.Parse(sem2marks.Text), 2));
            p.sem3percentage = (float)(Math.Round(float.Parse(sem3marks.Text), 2));
            
            try
            {
                SaveRest(p);
                DisplayAlert("Alert", "Record Saved Successfully", "OK");
                name.Text = "";
                stream.Text = "";
                college.Text = "";
                passoutyear.Text = "";
                phonenumber.Text = "";
                sem1marks.Text = "";
                sem2marks.Text = "";
                sem3marks.Text = "";
            }
            catch(Exception ex)
            {
                DisplayAlert("Alert", "Some Error Occurred", "OK");
            }
            
        }

        public bool ValidateInputs()
        {
            if (String.IsNullOrEmpty(name.Text))
            {
                DisplayAlert("Alert", "Please enter name", "OK");
                return false;
            }
            if (String.IsNullOrEmpty(stream.Text))
            {
                DisplayAlert("Alert", "Please enter stream", "OK");
                return false;
            }
            if (String.IsNullOrEmpty(college.Text))
            {
                DisplayAlert("Alert", "Please enter college name", "OK");
                return false;
            }
            if (String.IsNullOrEmpty(phonenumber.Text))
            {
                DisplayAlert("Alert", "Please enter phone number", "OK");
                return false;
            }
            if (String.IsNullOrEmpty(passoutyear.Text))
            {
                DisplayAlert("Alert", "Please enter pass out year", "OK");
                return false;
            }
            if (String.IsNullOrEmpty(sem1marks.Text))
            {
                DisplayAlert("Alert", "Please enter Semster 1 percentage", "OK");
                return false;
            }
            if (String.IsNullOrEmpty(sem2marks.Text))
            {
                DisplayAlert("Alert", "Please enter Semester 2 percentage", "OK");
                return false;
            }
            if (String.IsNullOrEmpty(sem3marks.Text))
            {
                DisplayAlert("Alert", "Please enter Semster 3 percentage", "OK");
                return false;
            }

            if(name.Text.Length > 50)
            {
                DisplayAlert("Alert", "Name can be 50 characters only", "OK");
                return false;
            }

            if (college.Text.Length > 50)
            {
                DisplayAlert("Alert", "College Name can be 50 characters only", "OK");
                return false;
            }

            if (stream.Text.Length > 50)
            {
                DisplayAlert("Alert", "Stream can be 50 characters only", "OK");
                return false;
            }

            var isPhoneNumberNumeric = Int64.TryParse(phonenumber.Text, out Int64 o);
            if (!isPhoneNumberNumeric || (phonenumber.Text.Length < 10 || phonenumber.Text.Length > 10))
            {
                DisplayAlert("Alert", "Phone number can only contain 10 numeric digits", "OK");
                return false;
            }

            var isPassoutYearNumeric = int.TryParse(passoutyear.Text, out int p);
            if (!isPassoutYearNumeric || p < 2018 || passoutyear.Text.Length < 4 || passoutyear.Text.Length > 4)
            {
                DisplayAlert("Alert", "Pass Out Year must be 4 digit number greater than 2018", "OK");
                return false;
            }

            var isSem1PercentageNumeric = float.TryParse(sem1marks.Text, out float q);
            if (!isSem1PercentageNumeric || (q < 0 || q > 100))
            {
                DisplayAlert("Alert", "Semester 1 Percentage can only be decimal values between 0 to 100", "OK");
                return false;
            }

            var isSem2PercentageNumeric = float.TryParse(sem2marks.Text, out float r);
            if (!isSem2PercentageNumeric || (r < 0 || r > 100))
            {
                DisplayAlert("Alert", "Semester 2 Percentage can only be decimal values between 0 to 100", "OK");
                return false;
            }

            var isSem3PercentageNumeric = float.TryParse(sem3marks.Text, out float s);
            if (!isSem3PercentageNumeric || (s < 0 || s > 100))
            {
                DisplayAlert("Alert", "Semester 3 Percentage can only be decimal values between 0 to 100", "OK");
                return false;
            }
            return true;
        }

        public async void SaveRest(person p)
        {
            using (var client = new HttpClient())
            {
                string url = "http://172.16.12.196:51108/api/products";
                string json = JsonConvert.SerializeObject(p);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = null;
                response = await client.PostAsync(url, content);
            }
        }
    }

    public class person
    {
        public string name { get; set; }
        public string stream { get; set; }
        public Int64 phonenumber { get; set; }
        public string collegename { get; set; }
        public int passoutyear { get; set; }
        public float sem1percentage { get; set; }
        public float sem2percentage { get; set; }
        public float sem3percentage { get; set; }
    }
}
